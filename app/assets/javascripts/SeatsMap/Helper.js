"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Seat_1 = require("./Seat");
var Helper = /** @class */ (function () {
    function Helper() {
    }
    Helper.getMaxNumberOfTickets = function () {
        var element = document.getElementById("max-tickets");
        return Number(element.textContent.replace(/\s+/g, ''));
    };
    Helper.getUserFounds = function () {
        var element = document.getElementById("user-founds");
        return Number(element.textContent.replace(/\s+/g, ''));
    };
    Helper.initializeBuyButton = function (ticketsManager) {
        var buyButton = document.getElementById('buy_marked_tickets');
        buyButton.addEventListener('click', function () {
            ticketsManager.goToBuyForm();
        });
    };
    Helper.getEventId = function () {
        var pathname = window.location.pathname;
        var elements = pathname.split("/");
        return elements[elements.length - 1];
    };
    Helper.getSeatId = function (button) {
        var child = button.firstChild;
        return child.textContent.replace(/\s+/g, '').replace(/\[.*/g, '');
    };
    Helper.getSeatPrice = function (button) {
        var child = button.firstChild;
        return Number(child.textContent.replace(/\s+/g, '').replace(/.*\[|PLN]/g, ''));
    };
    Helper.initializeSeats = function (seatManager) {
        var result = [];
        var seatButtons = document.getElementsByClassName('seat_icon');
        for (var i = 0; i < seatButtons.length; i++) {
            var seatButton = seatButtons.item(i);
            if (seatButton) {
                var seatId = this.getSeatId(seatButton);
                var seatPrice = this.getSeatPrice(seatButton);
                result.push(new Seat_1.Seat(seatManager, seatButton, seatId, seatPrice));
            }
        }
        return result;
    };
    return Helper;
}());
exports.Helper = Helper;
//# sourceMappingURL=Helper.js.map