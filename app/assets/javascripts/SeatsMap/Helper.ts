import {TicketManager} from "./TicketManager";
import {SeatsManager} from "./SeatsManager";
import {Seat} from "./Seat";

export class Helper {
    static getMaxNumberOfTickets(): number {
        let element = document.getElementById("max-tickets");
        return Number(element.textContent.replace(/\s+/g, ''));
    }

    static getUserFounds(): number {
        let element = document.getElementById("user-founds");
        return Number(element.textContent.replace(/\s+/g, ''));
    }

    static initializeBuyButton(ticketsManager: TicketManager) {
        let buyButton = document.getElementById('buy_marked_tickets');
        buyButton.addEventListener('click', () => {
            ticketsManager.goToBuyForm();
        });
    }

    static getEventId(): string {
        let pathname = window.location.pathname;
        let elements = pathname.split("/");
        return elements[elements.length - 1];
    }

    static getSeatId(button: Element): string {
        let child = button.firstChild;
        return child.textContent.replace(/\s+/g, '').replace(/\[.*/g, '')
    }


    private static getSeatPrice(button: Element) : number {
        let child = button.firstChild;
        return Number(child.textContent.replace(/\s+/g, '').replace(/.*\[|PLN]/g, ''))
    }

    static initializeSeats(seatManager : SeatsManager): Array<Seat> {
        let result: Array<Seat> = [];
        let seatButtons = document.getElementsByClassName('seat_icon');
        for (let i = 0; i < seatButtons.length; i++) {
            let seatButton = seatButtons.item(i);
            if (seatButton) {
                let seatId = this.getSeatId(seatButton);
                let seatPrice = this.getSeatPrice(seatButton);
                result.push(new Seat(seatManager, seatButton, seatId, seatPrice));
            }
        }

        return result;
    }
}