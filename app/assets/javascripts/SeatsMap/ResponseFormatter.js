"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Helper_1 = require("./Helper");
var BuyRequestBuilder = /** @class */ (function () {
    function BuyRequestBuilder() {
    }
    BuyRequestBuilder.prototype.buildRequest = function (selectedSeats) {
        var eventId = Helper_1.Helper.getEventId();
        var ticketsPrice = String(selectedSeats.reduce(function (a, b) { return a + b.seatPrice; }, 0));
        return "/purchase?event_id=" + eventId + "&tickets_price=" + ticketsPrice + this.buildSeatsParameters(selectedSeats);
    };
    BuyRequestBuilder.prototype.buildSeatsParameters = function (seats) {
        var parametersString = "";
        seats.forEach(function (seat) {
            console.log(seat.id);
            parametersString += "&seat_ids[]=" + seat.id;
        });
        return parametersString;
    };
    return BuyRequestBuilder;
}());
exports.BuyRequestBuilder = BuyRequestBuilder;
//# sourceMappingURL=ResponseFormatter.js.map