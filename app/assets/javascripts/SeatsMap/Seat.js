"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Seat = /** @class */ (function () {
    function Seat(seatsManager, htmlElement, id, seatPrice) {
        this.seatsManager = seatsManager;
        this.htmlElement = htmlElement;
        this.id = id;
        this.seatPrice = seatPrice;
        this.isSelected = false;
        this.addClickHandler();
        this.isAvailable = Seat.isAvailable(htmlElement);
    }
    Seat.prototype.addClickHandler = function () {
        var _this = this;
        if (!this.htmlElement) {
            return;
        }
        this.htmlElement.addEventListener("click", function () {
            if (!_this.isSelected && !_this.seatsManager.canSelect(_this)) {
                if (_this.isAvailable) {
                    alert("Nie można kupić więcej niż 5 biletów na jedno wydarzenie");
                }
                return;
            }
            if (!_this.isSelected) {
                if (!_this.seatsManager.chargeUser(_this.seatPrice)) {
                    alert("Nie masz wystarczających środków na koncie");
                    return;
                }
            }
            else {
                _this.seatsManager.releaseUserFounds(_this.seatPrice);
            }
            _this.isSelected = !_this.isSelected;
            if (_this.isSelected) {
                _this.addClass('selected');
            }
            else {
                _this.removeClass('selected');
            }
        });
    };
    Seat.prototype.addClass = function (className) {
        this.htmlElement.classList.add(className);
    };
    Seat.prototype.removeClass = function (className) {
        this.htmlElement.classList.remove(className);
    };
    Seat.isAvailable = function (button) {
        return button.classList.contains('btn-success');
    };
    return Seat;
}());
exports.Seat = Seat;
//# sourceMappingURL=Seat.js.map