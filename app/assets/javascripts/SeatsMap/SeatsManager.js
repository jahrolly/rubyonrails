"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Helper_1 = require("./Helper");
var SeatsManager = /** @class */ (function () {
    function SeatsManager() {
        this.maxSelected = 5;
        this.userFounds = 0;
        this.seats = Helper_1.Helper.initializeSeats(this);
        this.maxSelected = Helper_1.Helper.getMaxNumberOfTickets();
        this.userFounds = Helper_1.Helper.getUserFounds();
    }
    SeatsManager.prototype.getSelected = function () {
        return this.seats.filter(function (seat) { return seat.isSelected; });
    };
    SeatsManager.prototype.canSelect = function (seat) {
        return this.getSelected().length < this.maxSelected && seat.isAvailable;
    };
    SeatsManager.prototype.chargeUser = function (seatPrice) {
        if (this.userFounds < seatPrice) {
            return false;
        }
        this.userFounds -= seatPrice;
        return true;
    };
    SeatsManager.prototype.releaseUserFounds = function (seatPrice) {
        this.userFounds += seatPrice;
    };
    return SeatsManager;
}());
exports.SeatsManager = SeatsManager;
//# sourceMappingURL=SeatsManager.js.map