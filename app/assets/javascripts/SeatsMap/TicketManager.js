"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ResponseFormatter_1 = require("./ResponseFormatter");
var Helper_1 = require("./Helper");
var TicketManager = /** @class */ (function () {
    function TicketManager(seatsManager) {
        this.seatsManager = seatsManager;
        Helper_1.Helper.initializeBuyButton(this);
    }
    TicketManager.prototype.goToBuyForm = function () {
        var selectedSeats = this.seatsManager.getSelected();
        if (selectedSeats.length < 1) {
            return;
        }
        var requestBuilder = new ResponseFormatter_1.BuyRequestBuilder();
        window.location.href = requestBuilder.buildRequest(selectedSeats);
    };
    return TicketManager;
}());
exports.TicketManager = TicketManager;
//# sourceMappingURL=TicketManager.js.map