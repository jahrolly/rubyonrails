class PaymentController < ApplicationController
  def charged
    @user = current_user
    amount = params[:amount]
    new_balance = @user.founds + amount.to_d
    if @user.update_attribute(:founds, new_balance)
      respond_to do |format|
        format.html {redirect_to '/', notice: 'Konto zostało zasilone środkami'}
      end
    end
  end
end
