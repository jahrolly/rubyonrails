class TicketsController < ApplicationController
  include EventsHelper, TicketsHelper
  before_action :authenticate_user!, except: [:index, :show]
  before_action :correct_user, only: [:edit, :update, :destroy]
  before_action :set_ticket, only: [:show, :edit, :update, :destroy]

  # GET /tickets
  # GET /tickets.json
  def index
    @tickets = Ticket.all
  end

  # GET /tickets/1
  # GET /tickets/1.json
  def show
  end

  # GET /tickets/new
  def new
    @ticket = Ticket.new
  end

  # GET /tickets/1/edit
  def edit
  end

  def purchase
  end


  # POST /tickets
  # POST /tickets.json
  def create
    user = current_user
    seats = ticket_params[:seat_id_seq].gsub(/\s+/m, ' ').strip.split(" ")
    name = ticket_params[:name]
    address = ticket_params[:address]
    price = ticket_params[:price]
    email_address = ticket_params[:email_address]
    phone = ticket_params[:phone]
    event_id = ticket_params[:event_id]
    user_id = user.id
    counter = 0
    event = get_event(event_id)
    max_tickets = 5 - get_current_user_tickets(event.tickets).length
    seats.each do |seat|
      if counter >= max_tickets
        raise "Limit biletów na wydarzenie został wykorzystany";
      end

      if price.to_d > user.founds
        raise "Brak wystarczających środków na koncie"
      end

      @ticket = Ticket.new({:seat_id_seq => seat, :event_id => event_id, :name => name, :email_address => email_address, :phone => phone, :user_id => user_id, :price => price, :address => address})
      if @ticket.save
        counter += 1
      else
        raise @ticket.errors.full_messages.inspect
      end
    end
    new_balance = user.founds - @ticket.price
    user.update_attribute(:founds, new_balance)
    respond_to do |format|
      format.html {redirect_to tickets_url, notice: 'Bilety zostały zamówione'}
      format.json {head :no_content}
    end
  end

  # PATCH/PUT /tickets/1
  # PATCH/PUT /tickets/1.json
  def update
    respond_to do |format|
      if @ticket.update(ticket_params)
        format.html { redirect_to @ticket, notice: 'Ticket was successfully updated.' }
        format.json { render :show, status: :ok, location: @ticket }
      else
        format.html { render :edit }
        format.json { render json: @ticket.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tickets/1
  # DELETE /tickets/1.json
  def destroy
    @ticket.destroy
    respond_to do |format|
      format.html { redirect_to tickets_url, notice: 'Ticket was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ticket
      @ticket = Ticket.find(params[:id])
    end


  def correct_user
    if current_user&.isAdmin
      return
    end
    @ticket = current_user.tickets.find_by(id: params[:id])
    redirect_to tickets_path, notice: "Nie jesteś uprawniony do edycji tego biletu" if @ticket.nil?
  end

    # Never trust parameters from the scary internet, only allow the white list through.
    def ticket_params
      params.require(:ticket).permit(:name, :seat_id_seq, :address, :price, :email_address, :phone, :event_id)
    end
end
