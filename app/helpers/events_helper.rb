module EventsHelper
  def get_event(event_id)
    Event.find_by(:id => event_id)
  end

  def get_seat_id(row, col)
    (64 + row).chr + col.to_s
  end
end
