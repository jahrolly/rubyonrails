module TicketsHelper
  def get_current_user_tickets(tickets)
    if current_user&.isAdmin
      return tickets
    end
    tickets.select {|ticket| ticket.user_id == current_user.id}
  end

  def determine_ticket_price(event_id)
    event = Event.find(event_id)
    price = event.price_low
    price
  end

end
