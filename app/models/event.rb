class Event < ApplicationRecord
  validates :artist, :presence => true
  validates :description, :presence => true
  validates :price_low, :presence => true
  validates :price_high, :presence => true
  validates :event_date, :presence => true
  has_many :tickets
  def event_date_not_from_past
    if event_date < Date.today
      errors.add('Data wydarzenia', 'nie może być z przeszłości')
    end
  end
  validates :seats_rows, :presence => {:message => 'Podaj ilość rzędów'}
  validates :seats_columns, :presence => {:message => 'Podaj ilość miejsc w rzędzie'}

  def is_seat_available(seat_id)
    !tickets.map {|ticket| ticket.seat_id_seq}.include? seat_id
  end


  def get_seat_price(row, col)
    if row > (seats_rows / 2)
      return price_low
    end
    if col < (seats_columns / 4) || col > (3 * seats_columns / 4)
      return (price_low + price_high) / 2
    end
    return price_high
  end

end