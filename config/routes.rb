Rails.application.routes.draw do
  root :to=> "pages#home"

  devise_for :users
  resources :tickets
  resources :events, :only => [:index, :new, :create, :show]

  get '/charge', to: 'payment#charge'
  get '/purchase', to: 'tickets#purchase'
  post '/charge', to: 'payment#charged'
  post '/purchase', to: 'tickets#purchaseTickets'
end
