class AddSeatsInfoToEvents < ActiveRecord::Migration[5.1]
  def change
    add_column :events, :seats_rows, :decimal
    add_column :events, :seats_columns, :decimal
  end
end
