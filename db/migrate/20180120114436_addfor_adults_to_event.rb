class AddforAdultsToEvent < ActiveRecord::Migration[5.1]
  def change
    add_column :events, :isForAdults, :boolean, default: false
  end
end
