import {Seat} from "./Seat";
import {TicketManager} from "./TicketManager";

export class BuyRequestBuilder {

    buildRequest(selectedSeats: Array<Seat>): string {
        let eventId = TicketManager.getEventId();
        return `/buy?event_id=${eventId}${this.buildSeatsParameters(selectedSeats)}`;
    }

    private buildSeatsParameters(seats: Array<Seat>): string {
        let parametersString = "";
        seats.forEach(seat => {
            console.log(seat.id);
            parametersString += `&seat_ids[]=${seat.id}`;
        });

        return parametersString;
    }
}