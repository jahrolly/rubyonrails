import {Seat} from "./Seat";
import {TicketManager} from "./TicketManager";
import {Helper} from "./Helper";

export class BuyRequestBuilder {

    buildRequest(selectedSeats: Array<Seat>): string {
        let eventId = Helper.getEventId();
        let ticketsPrice = String(selectedSeats.reduce((a, b) => a + b.seatPrice, 0))
        return `/purchase?event_id=${eventId}&tickets_price=${ticketsPrice}${this.buildSeatsParameters(selectedSeats)}`;
    }

    private buildSeatsParameters(seats: Array<Seat>): string {
        let parametersString = "";
        seats.forEach(seat => {
            console.log(seat.id);
            parametersString += `&seat_ids[]=${seat.id}`;
        });
        return parametersString;
    }
}