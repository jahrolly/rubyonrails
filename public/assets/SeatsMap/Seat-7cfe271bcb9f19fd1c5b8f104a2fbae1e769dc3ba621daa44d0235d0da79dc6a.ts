import {SeatsManager} from "./SeatsManager";

export class Seat {
    isSelected: boolean = false;
    isAvailable: boolean;

    constructor(private seatsManager: SeatsManager, private htmlElement: Element,
                public id: string, public seatPrice: number) {

        this.addClickHandler();
        this.isAvailable = Seat.isAvailable(htmlElement);
    }

    private addClickHandler() {
        if (!this.htmlElement) {
            return;
        }

        this.htmlElement.addEventListener("click", () => {
            if (!this.isSelected && !this.seatsManager.canSelect(this)) {
                if (this.isAvailable) {
                    alert("Nie można kupić więcej niż 5 biletów na jedno wydarzenie");
                }
                return;
            }

            if (!this.isSelected) {
                if (!this.seatsManager.chargeUser(this.seatPrice)) {
                    alert("Nie masz wystarczających środków na koncie");
                    return
                }
            } else {
                this.seatsManager.releaseUserFounds(this.seatPrice)
            }

            this.isSelected = !this.isSelected;
            if (this.isSelected) {
                this.addClass('selected');
            } else {
                this.removeClass('selected');
            }
        });
    }

    private addClass(className: string) {
        this.htmlElement.classList.add(className);
    }

    private removeClass(className: string) {
        this.htmlElement.classList.remove(className);
    }

    static isAvailable(button: Element): boolean {
        return button.classList.contains('btn-success');
    }
}