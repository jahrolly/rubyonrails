import {SeatsManager} from "./SeatsManager";

export class Seat {
    isSelected: boolean = false;
    isAvailable: boolean;

    constructor(private seatsManager: SeatsManager, private htmlElement: Element,
                public id: string) {

        this.addClickHandler();
        this.isAvailable = Seat.isAvailable(htmlElement);
    }

    private addClickHandler() {
        if (!this.htmlElement) {
            return;
        }

        this.htmlElement.addEventListener("click", () => {
            if (!this.isSelected && !this.seatsManager.canSelect(this)) {
                if (this.isAvailable) {
                    alert("Nie można kupić więcej niż 5 biletów na jedno wydarzenie");
                }
                return;
            }

            this.isSelected = !this.isSelected;
            if (this.isSelected) {
                this.addClass('selected');
            } else {
                this.removeClass('selected');
            }
        });
    }

    private addClass(className: string) {
        this.htmlElement.classList.add(className);
    }

    private removeClass(className: string) {
        this.htmlElement.classList.remove(className);
    }

    static isAvailable(button: Element): boolean {
        return button.classList.contains('btn-success');
    }

    static initializeSeats(seatManager : SeatsManager): Array<Seat> {
        let result: Array<Seat> = [];
        let seatButtons = document.getElementsByClassName('seat-button');
        for (let i = 0; i < seatButtons.length; i++) {
            let seatButton = seatButtons.item(i);
            if (seatButton) {
                let seatId = Seat.getSeatId(seatButton);
                if (!seatId) {
                    continue;
                }

                result.push(new Seat(seatManager, seatButton, seatId));
            }
        }

        return result;
    }

    static getSeatId(button: Element): string {
        if (!button) {
            return null;
        }

        let child = button.firstChild;
        if (!child) {
            return null;
        }

        return child.textContent.replace(/\s+/g, '')
    }
}