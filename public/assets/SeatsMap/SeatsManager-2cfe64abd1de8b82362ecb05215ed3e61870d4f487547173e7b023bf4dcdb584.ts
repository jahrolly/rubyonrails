import {Seat} from "./Seat";
import {Helper} from "./Helper";

export class SeatsManager {
    seats: Array<Seat>
    maxSelected: number = 5;
    userFounds: number = 0;

    constructor() {
        this.seats = Helper.initializeSeats(this);
        this.maxSelected = Helper.getMaxNumberOfTickets();
        this.userFounds = Helper.getUserFounds();
    }

    getSelected(): Array<Seat> {
        return this.seats.filter(seat => seat.isSelected);
    }

    canSelect(seat: Seat): boolean {
        return this.getSelected().length < this.maxSelected && seat.isAvailable;
    }

    chargeUser(seatPrice: number) {
        if (this.userFounds < seatPrice) {
            return false
        }
        this.userFounds -= seatPrice;
        return true;
    }

    releaseUserFounds(seatPrice: number) {
        this.userFounds += seatPrice;
    }
}