import {Seat} from "./Seat";

export class SeatsManager {
    seats: Array<Seat>
    maxSelected: number = 5;

    constructor() {
        this.seats = Seat.initializeSeats(this);
        this.maxSelected = SeatsManager.getMaxNumberOfTickets();
    }

    getSelected(): Array<Seat> {
        return this.seats.filter(seat => seat.isSelected);
    }

    canSelect(seat: Seat): boolean {
        return this.getSelected().length < this.maxSelected && seat.isAvailable;
    }

    static getMaxNumberOfTickets(): number {
        let element = document.getElementsByClassName("max-tickets")[0];
        if (!element) {
            return 5;
        }
        return Number(element.textContent.replace(/\s+/g, ''));
    }
}