import {SeatsManager} from "./SeatsManager";
import {BuyRequestBuilder} from "./BuyRequestBuilder";

export class TicketManager {
    constructor(private seatsManager: SeatsManager) {
        TicketManager.initializeBuyButton(this);
    }

    goToBuyForm() {
        let selectedSeats = this.seatsManager.getSelected();
        if (selectedSeats.length < 1) {
            return;
        }

        let requestBuilder = new BuyRequestBuilder();
        window.location.href = requestBuilder.buildRequest(selectedSeats);
    }

    static initializeBuyButton(ticketsManager: TicketManager) {
        let buyButton = document.getElementById('buy_marked_tickets');
        if (!buyButton) {
            return;
        }

        buyButton.addEventListener('click', () => {
            ticketsManager.goToBuyForm();
        });
    }

    static getEventId(): string {
        let pathname = window.location.pathname;
        let elements = pathname.split("/");

        return elements[elements.length - 1];
    }
}