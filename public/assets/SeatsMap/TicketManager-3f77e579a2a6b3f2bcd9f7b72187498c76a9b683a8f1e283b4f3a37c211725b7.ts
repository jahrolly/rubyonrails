import {SeatsManager} from "./SeatsManager";
import {BuyRequestBuilder} from "./ResponseFormatter";
import {Helper} from "./Helper";

export class TicketManager {
    constructor(private seatsManager: SeatsManager) {
        Helper.initializeBuyButton(this);
    }

    goToBuyForm() {
        let selectedSeats = this.seatsManager.getSelected();
        if (selectedSeats.length < 1) {
            return;
        }
        let requestBuilder = new BuyRequestBuilder();
        window.location.href = requestBuilder.buildRequest(selectedSeats);
    }
}