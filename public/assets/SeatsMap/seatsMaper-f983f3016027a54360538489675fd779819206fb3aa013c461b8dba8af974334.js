(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Seat_1 = require("./Seat");
var Helper = /** @class */ (function () {
    function Helper() {
    }
    Helper.getMaxNumberOfTickets = function () {
        var element = document.getElementById("max-tickets");
        return Number(element.textContent.replace(/\s+/g, ''));
    };
    Helper.getUserFounds = function () {
        var element = document.getElementById("user-founds");
        return Number(element.textContent.replace(/\s+/g, ''));
    };
    Helper.initializeBuyButton = function (ticketsManager) {
        var buyButton = document.getElementById('buy_marked_tickets');
        buyButton.addEventListener('click', function () {
            ticketsManager.goToBuyForm();
        });
    };
    Helper.getEventId = function () {
        var pathname = window.location.pathname;
        var elements = pathname.split("/");
        return elements[elements.length - 1];
    };
    Helper.getSeatId = function (button) {
        var child = button.firstChild;
        return child.textContent.replace(/\s+/g, '').replace(/\[.*/g, '');
    };
    Helper.getSeatPrice = function (button) {
        var child = button.firstChild;
        return Number(child.textContent.replace(/\s+/g, '').replace(/.*\[|PLN]/g, ''));
    };
    Helper.initializeSeats = function (seatManager) {
        var result = [];
        var seatButtons = document.getElementsByClassName('seat_icon');
        for (var i = 0; i < seatButtons.length; i++) {
            var seatButton = seatButtons.item(i);
            if (seatButton) {
                var seatId = this.getSeatId(seatButton);
                var seatPrice = this.getSeatPrice(seatButton);
                result.push(new Seat_1.Seat(seatManager, seatButton, seatId, seatPrice));
            }
        }
        return result;
    };
    return Helper;
}());
exports.Helper = Helper;

},{"./Seat":3}],2:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Helper_1 = require("./Helper");
var BuyRequestBuilder = /** @class */ (function () {
    function BuyRequestBuilder() {
    }
    BuyRequestBuilder.prototype.buildRequest = function (selectedSeats) {
        var eventId = Helper_1.Helper.getEventId();
        var ticketsPrice = String(selectedSeats.reduce(function (a, b) { return a + b.seatPrice; }, 0));
        return "/purchase?event_id=" + eventId + "&tickets_price=" + ticketsPrice + this.buildSeatsParameters(selectedSeats);
    };
    BuyRequestBuilder.prototype.buildSeatsParameters = function (seats) {
        var parametersString = "";
        seats.forEach(function (seat) {
            console.log(seat.id);
            parametersString += "&seat_ids[]=" + seat.id;
        });
        return parametersString;
    };
    return BuyRequestBuilder;
}());
exports.BuyRequestBuilder = BuyRequestBuilder;

},{"./Helper":1}],3:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Seat = /** @class */ (function () {
    function Seat(seatsManager, htmlElement, id, seatPrice) {
        this.seatsManager = seatsManager;
        this.htmlElement = htmlElement;
        this.id = id;
        this.seatPrice = seatPrice;
        this.isSelected = false;
        this.addClickHandler();
        this.isAvailable = Seat.isAvailable(htmlElement);
    }
    Seat.prototype.addClickHandler = function () {
        var _this = this;
        if (!this.htmlElement) {
            return;
        }
        this.htmlElement.addEventListener("click", function () {
            if (!_this.isSelected && !_this.seatsManager.canSelect(_this)) {
                if (_this.isAvailable) {
                    alert("Nie można kupić więcej niż 5 biletów na jedno wydarzenie");
                }
                return;
            }
            if (!_this.isSelected) {
                if (!_this.seatsManager.chargeUser(_this.seatPrice)) {
                    alert("Nie masz wystarczających środków na koncie");
                    return;
                }
            }
            else {
                _this.seatsManager.releaseUserFounds(_this.seatPrice);
            }
            _this.isSelected = !_this.isSelected;
            if (_this.isSelected) {
                _this.addClass('selected');
            }
            else {
                _this.removeClass('selected');
            }
        });
    };
    Seat.prototype.addClass = function (className) {
        this.htmlElement.classList.add(className);
    };
    Seat.prototype.removeClass = function (className) {
        this.htmlElement.classList.remove(className);
    };
    Seat.isAvailable = function (button) {
        return button.classList.contains('btn-success');
    };
    return Seat;
}());
exports.Seat = Seat;

},{}],4:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Helper_1 = require("./Helper");
var SeatsManager = /** @class */ (function () {
    function SeatsManager() {
        this.maxSelected = 5;
        this.userFounds = 0;
        this.seats = Helper_1.Helper.initializeSeats(this);
        this.maxSelected = Helper_1.Helper.getMaxNumberOfTickets();
        this.userFounds = Helper_1.Helper.getUserFounds();
    }
    SeatsManager.prototype.getSelected = function () {
        return this.seats.filter(function (seat) { return seat.isSelected; });
    };
    SeatsManager.prototype.canSelect = function (seat) {
        return this.getSelected().length < this.maxSelected && seat.isAvailable;
    };
    SeatsManager.prototype.chargeUser = function (seatPrice) {
        if (this.userFounds < seatPrice) {
            return false;
        }
        this.userFounds -= seatPrice;
        return true;
    };
    SeatsManager.prototype.releaseUserFounds = function (seatPrice) {
        this.userFounds += seatPrice;
    };
    return SeatsManager;
}());
exports.SeatsManager = SeatsManager;

},{"./Helper":1}],5:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ResponseFormatter_1 = require("./ResponseFormatter");
var Helper_1 = require("./Helper");
var TicketManager = /** @class */ (function () {
    function TicketManager(seatsManager) {
        this.seatsManager = seatsManager;
        Helper_1.Helper.initializeBuyButton(this);
    }
    TicketManager.prototype.goToBuyForm = function () {
        var selectedSeats = this.seatsManager.getSelected();
        if (selectedSeats.length < 1) {
            return;
        }
        var requestBuilder = new ResponseFormatter_1.BuyRequestBuilder();
        window.location.href = requestBuilder.buildRequest(selectedSeats);
    };
    return TicketManager;
}());
exports.TicketManager = TicketManager;

},{"./Helper":1,"./ResponseFormatter":2}],6:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var SeatsManager_1 = require("./SeatsManager");
var TicketManager_1 = require("./TicketManager");
var Helper_1 = require("./Helper");
var seatsManager = new SeatsManager_1.SeatsManager();
var ticketManager = new TicketManager_1.TicketManager(seatsManager);
console.log(Helper_1.Helper.getEventId());


},{"./Helper":1,"./SeatsManager":4,"./TicketManager":5}]},{},[6]);
